import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Diese Test Klasse testet die Genauigkeit von Pi, die Abweichung darf nicht höher als 5% sein.
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 *
 */
 
public class PiTest {
 
    @Test
    public void testBerechnePi() {
           Pi p = new Pi();
           assertEquals(3.14159265359,p.berechnenPi(10000000),0.05);
    }
 
	
}